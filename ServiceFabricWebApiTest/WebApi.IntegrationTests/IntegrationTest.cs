﻿using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace WebApi.IntegrationTests
{
    public abstract class IntegrationTest
    {
        protected HttpClient HttpClient { get; }

        protected IntegrationTest()
        {
            var testServer = 
                new TestServer(
                    new WebHostBuilder()
                        .UseStartup<Startup>());
            HttpClient = testServer.CreateClient();
        }
    }
}
