﻿using System.Net.Http;
using FluentAssertions;
using WebApi.IntegrationTests.TestSupport;
using Xunit;

namespace WebApi.IntegrationTests.Controllers.WeatherForecastControllerTests
{
    public static class GetTests
    {
        public class Given_A_Url_When_Getting
            : Given_When_Then_Test
        {
            private HttpResponseMessage _result;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _result = HttpClient.GetAsync("/weatherForecast").GetAwaiter().GetResult();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Response()
            {
                _result.Should().NotBeNull();
            }
        }
    }
}